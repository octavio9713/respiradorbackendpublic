package unrn.edu.ar.iot.dao;

import unrn.edu.ar.iot.generic.GenericDao;
import unrn.edu.ar.iot.model.Alerta;

public interface AlertaDAO extends GenericDao<Alerta, Long> {

    Alerta save(Alerta alerta);

}
