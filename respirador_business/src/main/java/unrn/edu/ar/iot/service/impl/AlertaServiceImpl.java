package unrn.edu.ar.iot.service.impl;

import org.springframework.transaction.annotation.Transactional;
import unrn.edu.ar.iot.dao.AlertaDAO;
import unrn.edu.ar.iot.dao.PacienteDAO;
import unrn.edu.ar.iot.model.Alerta;
import unrn.edu.ar.iot.model.Paciente;
import unrn.edu.ar.iot.service.AlertaService;
import unrn.edu.ar.iot.service.PacienteService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Named("alertaService")
public class AlertaServiceImpl extends GenericServiceImpl<Alerta> implements AlertaService {

    @Inject
    AlertaDAO entityDAO;


    @Override
    protected AlertaDAO getEntityDAO() {
        return entityDAO;
    }

    @Override
    public List<Alerta> getList(Integer page, Integer pagesize, Map<String, String> filters, String sortField, Boolean asc, boolean distinct) {
        return super.getList(page, pagesize, filters, sortField, asc, distinct);
    }

    @Override
    @Transactional
    public Long create(Alerta entity) {
        return super.create(entity);
    }

    @Override
    @Transactional
    public Alerta save(Alerta entity) {
        return entityDAO.save(entity);
    }

    @Override
    @Transactional
    public Long update(Alerta entity) {
        return super.update(entity);
    }

    @Override
    @Transactional
    public void delete(Alerta entity) {
        super.delete(entity);
    }

    @Override
    @Transactional
    public Alerta get(Long id) {
        return super.get(id);
    }

    @Override
    public List<Alerta> getAll() {
        return new ArrayList<>(getEntityDAO().findAll());
    }
}
