package unrn.edu.ar.iot.dao;

import unrn.edu.ar.iot.generic.GenericDao;
import unrn.edu.ar.iot.model.security.User;

import javax.persistence.NoResultException;

public interface UserDAO extends GenericDao<User, Long> {

	User getEntityByName(String username) throws NoResultException;

	User getEntityByMail(String mail);

}
