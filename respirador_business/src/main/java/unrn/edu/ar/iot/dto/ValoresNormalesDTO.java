package unrn.edu.ar.iot.dto;

import unrn.edu.ar.iot.model.Alerta;
import unrn.edu.ar.iot.model.ValoresNormales;

public class ValoresNormalesDTO extends AbstractDTO {

    private int presionSistolicaMinima;
    private int presionSistolicaMaxima;
    private int presionDiastolicaMinima;
    private int presionDiastolicaMaxima;
    private int pulsoMinimo;
    private int pulsoMaximo;
    private int oxigenoEnSangreMinimo;
    private int oxigenoEnSangreMaximo;
    private int respiracionMinima;
    private int respiracionMaxima;
    private float temperaturaMinima;
    private float temperaturaMaxima;

    public ValoresNormalesDTO() {
    }

    public ValoresNormalesDTO(Long id) {
        this.id = id;
    }

    public ValoresNormalesDTO(Long id, int presionSistolicaMinima, int presionSistolicaMaxima, int presionDiastolicaMinima,
                              int presionDiastolicaMaxima, int pulsoMinimo, int pulsoMaximo, int oxigenoEnSangreMinimo,
                              int oxigenoEnSangreMaximo, int respiracionMinima, int respiracionMaxima, float temperaturaMinima,
                              float temperaturaMaxima) {
        this.setId(id);
        this.presionSistolicaMinima = presionSistolicaMinima;
        this.presionSistolicaMaxima = presionSistolicaMaxima;
        this.presionDiastolicaMinima = presionDiastolicaMinima;
        this.presionDiastolicaMaxima = presionDiastolicaMaxima;
        this.pulsoMinimo = pulsoMinimo;
        this.pulsoMaximo = pulsoMaximo;
        this.oxigenoEnSangreMinimo = oxigenoEnSangreMinimo;
        this.oxigenoEnSangreMaximo = oxigenoEnSangreMaximo;
        this.respiracionMinima = respiracionMinima;
        this.respiracionMaxima = respiracionMaxima;
        this.temperaturaMinima = temperaturaMinima;
        this.temperaturaMaxima = temperaturaMaxima;
    }

    @Override
    public ValoresNormales convertToEntity() {

        ValoresNormales valoresNormales = new ValoresNormales(getId());

        valoresNormales.setPresionSistolicaMinima(presionSistolicaMinima);
        valoresNormales.setPresionSistolicaMaxima(presionSistolicaMaxima);
        valoresNormales.setPresionDiastolicaMinima(presionDiastolicaMinima);
        valoresNormales.setPresionDiastolicaMaxima(presionDiastolicaMaxima);
        valoresNormales.setPulsoMinimo(pulsoMinimo);
        valoresNormales.setPulsoMaximo(pulsoMaximo);
        valoresNormales.setOxigenoEnSangreMinimo(oxigenoEnSangreMinimo);
        valoresNormales.setOxigenoEnSangreMaximo(oxigenoEnSangreMaximo);
        valoresNormales.setRespiracionMinima(respiracionMinima);
        valoresNormales.setRespiracionMaxima(respiracionMaxima);
        valoresNormales.setTemperaturaMinima(temperaturaMinima);
        valoresNormales.setTemperaturaMaxima(temperaturaMaxima);

        return valoresNormales;
    }

    public int getPresionSistolicaMinima() {
        return presionSistolicaMinima;
    }

    public void setPresionSistolicaMinima(int presionSistolicaMinima) {
        this.presionSistolicaMinima = presionSistolicaMinima;
    }

    public int getPresionSistolicaMaxima() {
        return presionSistolicaMaxima;
    }

    public void setPresionSistolicaMaxima(int presionSistolicaMaxima) {
        this.presionSistolicaMaxima = presionSistolicaMaxima;
    }

    public int getPresionDiastolicaMinima() {
        return presionDiastolicaMinima;
    }

    public void setPresionDiastolicaMinima(int presionDiastolicaMinima) {
        this.presionDiastolicaMinima = presionDiastolicaMinima;
    }

    public int getPresionDiastolicaMaxima() {
        return presionDiastolicaMaxima;
    }

    public void setPresionDiastolicaMaxima(int presionDiastolicaMaxima) {
        this.presionDiastolicaMaxima = presionDiastolicaMaxima;
    }

    public int getPulsoMinimo() {
        return pulsoMinimo;
    }

    public void setPulsoMinimo(int pulsoMinimo) {
        this.pulsoMinimo = pulsoMinimo;
    }

    public int getPulsoMaximo() {
        return pulsoMaximo;
    }

    public void setPulsoMaximo(int pulsoMaximo) {
        this.pulsoMaximo = pulsoMaximo;
    }

    public float getOxigenoEnSangreMinimo() {
        return oxigenoEnSangreMinimo;
    }

    public void setOxigenoEnSangreMinimo(int oxigenoEnSangreMinimo) {
        this.oxigenoEnSangreMinimo = oxigenoEnSangreMinimo;
    }

    public int getOxigenoEnSangreMaximo() {
        return oxigenoEnSangreMaximo;
    }

    public void setOxigenoEnSangreMaximo(int oxigenoEnSangreMaximo) {
        this.oxigenoEnSangreMaximo = oxigenoEnSangreMaximo;
    }

    public int getRespiracionMinima() {
        return respiracionMinima;
    }

    public void setRespiracionMinima(int respiracionMinima) {
        this.respiracionMinima = respiracionMinima;
    }

    public int getRespiracionMaxima() {
        return respiracionMaxima;
    }

    public void setRespiracionMaxima(int respiracionMaxima) {
        this.respiracionMaxima = respiracionMaxima;
    }

    public float getTemperaturaMinima() {
        return temperaturaMinima;
    }

    public void setTemperaturaMinima(float temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    public float getTemperaturaMaxima() {
        return temperaturaMaxima;
    }

    public void setTemperaturaMaxima(float temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
    }
}
