package unrn.edu.ar.iot.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProfileFilter {

    private Long groupID;
    private Boolean responsable;

    public ProfileFilter() {
    }

    public ProfileFilter(Long groupID, Boolean responsable) {
        this.groupID = groupID;
        this.responsable = responsable;
    }

    public Long getGroupID() {
        return groupID;
    }

    public void setGroupID(Long groupID) {
        this.groupID = groupID;
    }

    public Boolean getResponsable() {
        return responsable;
    }

    public void setResponsable(Boolean responsable) {
        this.responsable = responsable;
    }

    /**
     * Recibe una cadena de filtros y la convierte en objetos. El formato de la cadena es: groupID-isResponsable|groupID-isResponsable|
     */
    public static List<ProfileFilter> parseFilters(String stringList){
        //final ArrayList<ProfileFilter> filtersList = new ArrayList<>();

        if ( stringList.length() > 1 ){
            String[] filters = stringList.split("/");
            return Arrays.stream(filters).map(s -> {
                String[] object = s.split("-");
                return new ProfileFilter( Long.valueOf( object[0] ), Boolean.valueOf( object[1] ) ) ;
            }).collect(Collectors.toList());
            /*
            Arrays.stream(filters).forEach( s -> {
                String[] object = s.split("-");
                filtersList.add(new ProfileFilter( Long.valueOf( object[0] ), Boolean.valueOf( object[1] ) ) );
            });
            */
        }

        return new ArrayList<>();
    }
}