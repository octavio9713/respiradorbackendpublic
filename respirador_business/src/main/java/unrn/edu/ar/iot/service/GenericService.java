package unrn.edu.ar.iot.service;

import unrn.edu.ar.iot.model.GenericEntity;

import java.util.List;
import java.util.Map;

public interface GenericService<Entity extends GenericEntity> {

    Long create(Entity entity);

    Long update(Entity entity);

    void delete(Long id);

    Entity get(Long id);

    void delete(Entity entity);

    Long getCount(Map<String, String> filters);

    List<Entity> getList(Integer page, Integer pagesize, Map<String, String> filters, String sortField, Boolean asc, boolean distinct);

}
