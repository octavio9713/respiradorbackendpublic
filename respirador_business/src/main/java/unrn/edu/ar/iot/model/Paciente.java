package unrn.edu.ar.iot.model;

import unrn.edu.ar.iot.dto.PacienteDTO;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "paciente")
public class Paciente extends AbstractEntity {

    private String nombres;
    private String apellidos;

    @Column(name = "numero_paciente")
    private Long numeroPaciente;
    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;
    private String genero;

    @OneToMany(
            mappedBy = "paciente",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Fetch(FetchMode.JOIN)
    private Set<PeriodoDeAtencion> periodosDeAtencion = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "id_valores_normales")
    private ValoresNormales valoresNormales;

    public Paciente(){};

    public Paciente(Long id) {
        this.setId(id);
    }

    @Transient
    public PacienteDTO convertToDTO() {
        PacienteDTO dto = new PacienteDTO(this.getId(), this.getNombres(), this.getApellidos(),
                this.getNumeroPaciente(), this.getGenero(),
                this.getFechaNacimiento(), this.getValoresNormales());

        return dto;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Long getNumeroPaciente() {
        return numeroPaciente;
    }

    public void setNumeroPaciente(Long numeroPaciente) {
        this.numeroPaciente = numeroPaciente;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public ValoresNormales getValoresNormales() {
        return valoresNormales;
    }

    public void setValoresNormales(ValoresNormales valoresNormales) {
        this.valoresNormales = valoresNormales;
    }
}
