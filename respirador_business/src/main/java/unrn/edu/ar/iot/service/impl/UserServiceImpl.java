package unrn.edu.ar.iot.service.impl;

import unrn.edu.ar.iot.dao.UserDAO;
import unrn.edu.ar.iot.exception.AccessDeniedException;
import unrn.edu.ar.iot.exception.BusinessException;
import unrn.edu.ar.iot.logger.Log;
import unrn.edu.ar.iot.model.security.User;
import unrn.edu.ar.iot.service.UserService;
import unrn.edu.ar.iot.util.Constantes;
import org.slf4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.stream.Collectors;

@Named("userService")
public class UserServiceImpl extends GenericServiceImpl<User> implements UserService {

    @Inject
    UserDAO entityDAO;

    @Inject
    BCryptPasswordEncoder passwordEncoder;

    static @Log
    Logger LOG;


    public UserDAO getEntityDAO() {
        return entityDAO;
    }

    @Override
    @Transactional
    public Long create(User entity) {
        encriptarClave(entity);
        return super.create(entity);
    }

    private User findByName(String username) {
        User usuario = null;
        try {
            usuario = entityDAO.getEntityByName(username);

        } catch (NoResultException e) {

            if (usuario == null) {
                throw new BusinessException(e.getMessage(), e, "usuarioNoExiste");
            }
        }

        return usuario;

    }

    public User validationByUsername(String username) {
        User usuario = null;
        try {
            usuario = entityDAO.getEntityByName(username);

        } catch (NoResultException e) {

            if (usuario == null) {
                throw new AccessDeniedException(e.getMessage(), e, "UsuarioAccessDenied");
            }
        }

        return usuario;

    }

    public User findByMail(String mail) {
        User usuario = null;
        try {
            usuario = entityDAO.getEntityByMail(mail);

        } catch (NoResultException e) {

            if (usuario == null) {
                // throw new UsernameNotFoundException("Usuario no existe.");
                throw new BusinessException(e.getMessage(), e, "usuarioNoExiste");
            }
        }

        return usuario;

    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void cambiarClave(User user) {
        encriptarClave(user);
        getEntityDAO().update(user);
    }

    private void encriptarClave(User t) {
        if (t.getPassword() == null) {
            t.setPassword(Constantes.GLOBAL_CLAVE);
        }

        t.setPassword(passwordEncoder.encode(t.getPassword()));
    }

    @Override
    public boolean checkPassword(String newPassword) {
        return passwordEncoder.matches( newPassword, getLoggedUser().getPassword() );
    }



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails userReturn = findByName(username);
        return userReturn;
    }

    @Override
    public User findByUsername(String username) {
        return findByName(username);
    }

    @Override
    public boolean isUserExist(User user) {
        if (get(user.getId()) != null)
            return true;
        return false;
    }


    @Override
    public User getLoggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (User) authentication.getPrincipal();

    }

    @Override
    public String idLoggedUser() {
        return getLoggedUser()!= null ? getLoggedUser().getId().toString():null;
    }

    @Override
    public List<User> findAll() {
        return entityDAO.findAll();
    }

}