package unrn.edu.ar.iot.dao;

import unrn.edu.ar.iot.generic.GenericDao;
import unrn.edu.ar.iot.model.PeriodoDeAtencion;

import java.util.List;

public interface PeriodoDeAtencionDAO extends GenericDao<PeriodoDeAtencion, Long> {

    PeriodoDeAtencion save(PeriodoDeAtencion periodoDeAtencion);

    List<PeriodoDeAtencion> findByPacienteId(Long id);

    List findByPacienteNumber(Long number);

}
