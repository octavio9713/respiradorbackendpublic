package unrn.edu.ar.iot.service.impl;

import org.springframework.transaction.annotation.Transactional;
import unrn.edu.ar.iot.dao.PeriodoDeAtencionDAO;
import unrn.edu.ar.iot.model.PeriodoDeAtencion;
import unrn.edu.ar.iot.service.PeriodoDeAtencionService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Named("periodoDeAtencionService")
public class PeriodoDeAtencionServiceImpl extends GenericServiceImpl<PeriodoDeAtencion> implements PeriodoDeAtencionService {

    @Inject
    PeriodoDeAtencionDAO entityDAO;


    @Override
    protected PeriodoDeAtencionDAO getEntityDAO() {
        return entityDAO;
    }

    @Override
    public List<PeriodoDeAtencion> getList(Integer page, Integer pagesize, Map<String, String> filters, String sortField, Boolean asc, boolean distinct) {
        return super.getList(page, pagesize, filters, sortField, asc, distinct);
    }

    @Override
    @Transactional
    public Long create(PeriodoDeAtencion entity) {
        return super.create(entity);
    }

    @Override
    @Transactional
    public PeriodoDeAtencion save(PeriodoDeAtencion entity) {
        return entityDAO.save(entity);
    }

    @Override
    @Transactional
    public Long update(PeriodoDeAtencion entity) {
        return super.update(entity);
    }

    @Override
    @Transactional
    public void delete(PeriodoDeAtencion entity) {
        super.delete(entity);
    }

    @Override
    @Transactional
    public PeriodoDeAtencion get(Long id) {
        return super.get(id);
    }

    @Override
    public List<PeriodoDeAtencion> getAll() {
        return new ArrayList<>(getEntityDAO().findAll());
    }

    @Override
    public List<PeriodoDeAtencion> getByIdPaciente(Long id) {
        return new ArrayList<>(getEntityDAO().findByPacienteId(id));
    }

    @Override
    public List<PeriodoDeAtencion> getByNumberPaciente(Long number) {
        return new ArrayList<>(getEntityDAO().findByPacienteNumber(number));
    }
}
