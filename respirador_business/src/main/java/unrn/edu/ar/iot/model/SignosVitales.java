package unrn.edu.ar.iot.model;

import com.google.api.client.util.DateTime;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import unrn.edu.ar.iot.dto.SignosVitalesDTO;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "signos_vitales")
public class SignosVitales extends AbstractEntity {

    @Column (name = "fecha_obtencion")
    private LocalDateTime fechaObtencion;

    @Column(name = "presion_sistolica")
    private int presionSistolica;

    @Column(name = "presion_diastolica")
    private int presionDiastolica;

    private int pulso;

    @Column(name = "oxigeno_en_sangre")
    private int oxigenoEnSangre;

    private int respiracion;
    private Float temperatura;

    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "id_periodo")
    private PeriodoDeAtencion periodoDeAtencion;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "signos_vitales_alertas",
            joinColumns = {@JoinColumn(name = "id_signos_vitales", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "id_alerta", referencedColumnName = "id")}
    )
    private Set<Alerta> alertas = new HashSet<>(0);

    public SignosVitales() {
    }

    public SignosVitales(Long id) {
        this.setId(id);
    }

    @Transient
    public SignosVitalesDTO convertToDTO() {
        SignosVitalesDTO dto = new SignosVitalesDTO(this.getId(), this.presionSistolica, this.presionDiastolica,
                this.pulso, this.oxigenoEnSangre, this.respiracion, this.temperatura, this.fechaObtencion, null);

        return dto;
    }

    @Transient
    public SignosVitalesDTO convertToAllDTO() {

        SignosVitalesDTO dto = this.convertToDTO();
        dto.setAlertas(this.getAlertas().stream().map(Alerta::convertToDTO).collect(Collectors.toList()));

        return dto;
    }

    public LocalDateTime getFechaObtencion() {
        return fechaObtencion;
    }

    public void setFechaObtencion(LocalDateTime fechaObtencion) {
        this.fechaObtencion = fechaObtencion;
    }

    public int getPresionSistolica() {
        return presionSistolica;
    }

    public void setPresionSistolica(int presionSistolica) {
        this.presionSistolica = presionSistolica;
    }

    public int getPresionDiastolica() {
        return presionDiastolica;
    }

    public void setPresionDiastolica(int presionDiastolica) {
        this.presionDiastolica = presionDiastolica;
    }

    public int getPulso() {
        return pulso;
    }

    public void setPulso(int pulso) {
        this.pulso = pulso;
    }

    public int getOxigenoEnSangre() {
        return oxigenoEnSangre;
    }

    public void setOxigenoEnSangre(int oxigenoEnSangre) {
        this.oxigenoEnSangre = oxigenoEnSangre;
    }

    public PeriodoDeAtencion getPeriodoDeAtencion() {
        return periodoDeAtencion;
    }

    public void setPeriodoDeAtencion(PeriodoDeAtencion periodoDeAtencion) {
        this.periodoDeAtencion = periodoDeAtencion;
    }

    public int getRespiracion() {
        return respiracion;
    }

    public void setRespiracion(int respiracion) {
        this.respiracion = respiracion;
    }

    public Float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Float temperatura) {
        this.temperatura = temperatura;
    }

    public void setAlertas(List<Alerta> questions) {
        this.alertas = new HashSet<>(alertas);
    }

    public List<Alerta> getAlertas(){

        List<Alerta> alertas =  this.alertas.stream()
                .collect(Collectors.toList());

        return alertas;
    }
}
