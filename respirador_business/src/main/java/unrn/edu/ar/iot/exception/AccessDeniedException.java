package unrn.edu.ar.iot.exception;

/**
 * Clase base para las excepciones de negocio
 * 
 */
public class AccessDeniedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String keyMessage;

	public AccessDeniedException() {
		// void
	}

	public AccessDeniedException(final String message, Throwable t, String keyMessage) {
		super(message, t);
		this.keyMessage = keyMessage;
	}

	public String getKeyMessage() {
		return keyMessage;
	}

	public void setKeyMessage(String keyMessage) {
		this.keyMessage = keyMessage;
	}

}
