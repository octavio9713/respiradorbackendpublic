package unrn.edu.ar.iot.service;

import unrn.edu.ar.iot.model.PeriodoDeAtencion;

import java.util.List;

public interface PeriodoDeAtencionService extends GenericService<PeriodoDeAtencion> {

    PeriodoDeAtencion save(PeriodoDeAtencion entity);

    List<PeriodoDeAtencion> getAll();

    List<PeriodoDeAtencion> getByIdPaciente(Long idPaciente);

    List<PeriodoDeAtencion> getByNumberPaciente(Long numberPaciente);


}
