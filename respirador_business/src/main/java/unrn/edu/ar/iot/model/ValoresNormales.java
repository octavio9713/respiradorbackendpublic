package unrn.edu.ar.iot.model;

import unrn.edu.ar.iot.dto.ValoresNormalesDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "valores_normales")
public class ValoresNormales extends AbstractEntity {

    @Column(name = "presion_sistolica_minima")
    private int presionSistolicaMinima;
    @Column(name = "presion_sistolica_maxima")
    private int presionSistolicaMaxima;
    @Column(name = "presion_diastolica_minima")
    private int presionDiastolicaMinima;
    @Column(name = "presion_diastolica_maxima")
    private int presionDiastolicaMaxima;
    @Column(name = "pulso_minimo")
    private int pulsoMinimo;
    @Column(name = "pulso_maximo")
    private int pulsoMaximo;
    @Column(name = "oxigeno_en_sangre_minimo")
    private int oxigenoEnSangreMinimo;
    @Column(name = "oxigeno_en_sangre_maximo")
    private int oxigenoEnSangreMaximo;
    @Column(name = "respiracion_minima")
    private int respiracionMinima;
    @Column(name = "respiracion_maxima")
    private int respiracionMaxima;
    @Column(name = "temperatura_minima")
    private float temperaturaMinima;
    @Column(name = "temperatura_maxima")
    private float temperaturaMaxima;

    public ValoresNormales() {
    }

    public ValoresNormales(Long id) {
        this.setId(id);
    }

    @Transient
    public ValoresNormalesDTO convertToDTO() {
        ValoresNormalesDTO dto = new ValoresNormalesDTO(this.getId(), this.getPresionSistolicaMinima(),
                this.getPresionSistolicaMaxima(),
                this.getPresionDiastolicaMinima(), this.getPresionDiastolicaMaxima(), this.getPulsoMinimo(),
                this.getPulsoMaximo(), this.getOxigenoEnSangreMinimo(), this.getOxigenoEnSangreMaximo(),
                this.getRespiracionMinima(), this.getRespiracionMaxima(), this.getTemperaturaMinima(),
                this.getTemperaturaMaxima());

        return dto;
    }

    public int getPresionSistolicaMinima() {
        return presionSistolicaMinima;
    }

    public void setPresionSistolicaMinima(int presionSistolicaMinima) {
        this.presionSistolicaMinima = presionSistolicaMinima;
    }

    public int getPresionSistolicaMaxima() {
        return presionSistolicaMaxima;
    }

    public void setPresionSistolicaMaxima(int presionSistolicaMaxima) {
        this.presionSistolicaMaxima = presionSistolicaMaxima;
    }

    public int getPresionDiastolicaMinima() {
        return presionDiastolicaMinima;
    }

    public void setPresionDiastolicaMinima(int presionDiastolicaMinima) {
        this.presionDiastolicaMinima = presionDiastolicaMinima;
    }

    public int getPresionDiastolicaMaxima() {
        return presionDiastolicaMaxima;
    }

    public void setPresionDiastolicaMaxima(int presionDiastolicaMaxima) {
        this.presionDiastolicaMaxima = presionDiastolicaMaxima;
    }

    public int getPulsoMinimo() {
        return pulsoMinimo;
    }

    public void setPulsoMinimo(int pulsoMinimo) {
        this.pulsoMinimo = pulsoMinimo;
    }

    public int getPulsoMaximo() {
        return pulsoMaximo;
    }

    public void setPulsoMaximo(int pulsoMaximo) {
        this.pulsoMaximo = pulsoMaximo;
    }

    public int getOxigenoEnSangreMinimo() {
        return oxigenoEnSangreMinimo;
    }

    public void setOxigenoEnSangreMinimo(int oxigenoEnSangreMinimo) {
        this.oxigenoEnSangreMinimo = oxigenoEnSangreMinimo;
    }

    public int getOxigenoEnSangreMaximo() {
        return oxigenoEnSangreMaximo;
    }

    public void setOxigenoEnSangreMaximo(int oxigenoEnSangreMaximo) {
        this.oxigenoEnSangreMaximo = oxigenoEnSangreMaximo;
    }

    public int getRespiracionMinima() {
        return respiracionMinima;
    }

    public void setRespiracionMinima(int respiracionMinima) {
        this.respiracionMinima = respiracionMinima;
    }

    public int getRespiracionMaxima() {
        return respiracionMaxima;
    }

    public void setRespiracionMaxima(int respiracionMaxima) {
        this.respiracionMaxima = respiracionMaxima;
    }

    public float getTemperaturaMinima() {
        return temperaturaMinima;
    }

    public void setTemperaturaMinima(float temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    public float getTemperaturaMaxima() {
        return temperaturaMaxima;
    }

    public void setTemperaturaMaxima(float temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
    }
}
