package unrn.edu.ar.iot.dto;

import unrn.edu.ar.iot.model.security.User;

public class UserDTO extends AbstractDTO {

    private String username;
    private String name;
    private String email;

    public UserDTO() {
    }

    public UserDTO(Long id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public User convertToEntity() {
        return new User(getId());
    }
}
