package unrn.edu.ar.iot.service;

import unrn.edu.ar.iot.model.ValoresNormales;

import java.util.List;

public interface ValoresNormalesService extends GenericService<ValoresNormales> {

    ValoresNormales save(ValoresNormales entity);

    List<ValoresNormales> getAll();

}
