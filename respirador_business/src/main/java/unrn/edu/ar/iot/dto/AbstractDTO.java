package unrn.edu.ar.iot.dto;

import unrn.edu.ar.iot.model.AbstractEntity;

public abstract class AbstractDTO {

    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public abstract AbstractEntity convertToEntity();
}
