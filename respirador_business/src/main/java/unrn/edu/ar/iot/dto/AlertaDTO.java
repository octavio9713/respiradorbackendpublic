package unrn.edu.ar.iot.dto;

import unrn.edu.ar.iot.model.Alerta;
import unrn.edu.ar.iot.model.Paciente;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AlertaDTO extends AbstractDTO {


    private String signo;
    private String motivo;
    private String mensajeDeAlerta;

    public AlertaDTO() {
    }

    public AlertaDTO(Long id) {
        this.id = id;
    }

    public AlertaDTO(Long id, String signo, String motivo, String mensajeDeAlerta) {
        this.id = id;
        this.signo = signo;
        this.motivo = motivo;
        this.mensajeDeAlerta = mensajeDeAlerta;
    }

    @Override
    public Alerta convertToEntity() {

        Alerta alerta = new Alerta(getId());
        alerta.setSigno(this.signo);
        alerta.setMotivo(this.motivo);
        alerta.setMensajeDeAlerta(this.mensajeDeAlerta);
        return alerta;
    }

    public String getSigno() {
        return signo;
    }

    public void setSigno(String signo) {
        this.signo = signo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getMensajeDeAlerta() {
        return mensajeDeAlerta;
    }

    public void setMensajeDeAlerta(String mensajeDeAlerta) {
        this.mensajeDeAlerta = mensajeDeAlerta;
    }
}
