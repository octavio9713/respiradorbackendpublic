package unrn.edu.ar.iot.dao;

import unrn.edu.ar.iot.generic.GenericDao;
import unrn.edu.ar.iot.model.ValoresNormales;

public interface ValoresNormalesDAO extends GenericDao<ValoresNormales, Long> {

    ValoresNormales save(ValoresNormales valoresNormales);

}
