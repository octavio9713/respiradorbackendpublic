package unrn.edu.ar.iot.service;

import unrn.edu.ar.iot.model.Paciente;

import java.util.List;

public interface PacienteService extends GenericService<Paciente> {

    Paciente save(Paciente entity);

    List<Paciente> getAll();

    Paciente getByPatientNumber(long number);


}
