package unrn.edu.ar.iot.service.impl;

import unrn.edu.ar.iot.generic.GenericDao;
import unrn.edu.ar.iot.model.AbstractEntity;
import unrn.edu.ar.iot.service.GenericService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


public abstract class GenericServiceImpl<E extends AbstractEntity>
        implements GenericService<E> {

    final static Logger LOG = LoggerFactory.getLogger(GenericServiceImpl.class);

    protected abstract GenericDao<E, Long> getEntityDAO();

    @Transactional
    public Long create(E entity) {
        LOG.info("CREATE {}", entity);
        return getEntityDAO().create(entity);
    }

    @Transactional
    public Long update(E entity) {
        LOG.info("UPDATE {}", entity);
        return getEntityDAO().update(entity).getId();
    }

    @Override
    @Transactional
    public void delete(Long id) {
        LOG.info("DELETE {}", id);
        E entity = getEntityDAO().read(id);
        getEntityDAO().delete(entity);
    }

    @Override
    @Transactional
    public void delete(E entity) {
        LOG.info("DELETE {}");
        getEntityDAO().delete(entity);
    }

    public E get(Long id) {
        LOG.debug("GET {}", id);
        return getEntityDAO().read(id);
    }

    @Transactional(readOnly = true)
    public Long getCount(Map<String, String> filters) {
        return getEntityDAO().count(getEntityDAO().getSearchPredicates(getEntityDAO().rootCount(), filters));
    }

    @Transactional(readOnly = true)
    public List<E> getList(Integer page, Integer pagesize, Map<String, String> filters, String sortField,
                           Boolean asc, boolean distinct) {
        return getEntityDAO().listwithPag(getEntityDAO().getSearchPredicates(getEntityDAO().rootCount(), filters), page,
                pagesize, sortField, asc, distinct);
    }

}
