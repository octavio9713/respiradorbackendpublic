package unrn.edu.ar.iot.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LocationFilter {

    private Long docketTypeID;
    private List<Long> locationsID = new ArrayList<>();

    public LocationFilter() {
    }

    public LocationFilter(Long docketTypeID, List<Long> locationsID) {
        this.docketTypeID = docketTypeID;
        this.locationsID = locationsID;
    }

    public Long getDocketTypeID() {
        return docketTypeID;
    }

    public void setDocketTypeID(Long docketTypeID) {
        this.docketTypeID = docketTypeID;
    }

    public List<Long> getLocationsID() {
        return locationsID;
    }

    public void setLocationsID(List<Long> locationsID) {
        this.locationsID = locationsID;
    }

    /**
     * Recibe una cadena de filtros y la convierte en objetos. El formato de la cadena es: docketTypeID-locationID,locationID|
     */
    public static List<LocationFilter> parseFilters(String stringList){
        if ( stringList.length() > 1 ){
            String[] filters = stringList.split("/");
            return Arrays.stream(filters).map(s -> {
                String[] object = s.split("-");
                List<Long> locationsID = object[1].equals("null") ?
                        new ArrayList<>() :
                        Arrays.stream(object[1].split(","))
                        .map(id -> Long.valueOf(id))
                        .collect(Collectors.toList());
                return new LocationFilter( Long.valueOf( object[0] ), locationsID ) ;
            }).collect(Collectors.toList());
        }

        return new ArrayList<>();
    }


}