package unrn.edu.ar.iot.dto;

import unrn.edu.ar.iot.model.Paciente;
import unrn.edu.ar.iot.model.ValoresNormales;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PacienteDTO extends AbstractDTO {

    private String nombres;
    private String apellidos;
    private Long numeroPaciente;
    private String fechaNacimiento;
    private String genero;

    private ValoresNormalesDTO valoresNormalesDTO;

    private DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public PacienteDTO() {
    }

    public PacienteDTO(Long id) {
        this.id = id;
    }

    public PacienteDTO(Long id, String nombres, String apellidos, Long numeroPaciente, String genero,
                       LocalDate fechaNacimiento, ValoresNormales valoresNormales) {

        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.numeroPaciente = numeroPaciente;
        this.fechaNacimiento = fechaNacimiento.format(dateFormat);
        this.genero = genero;
        if (valoresNormales != null)
            this.valoresNormalesDTO = valoresNormales.convertToDTO();
    }

    @Override
    public Paciente convertToEntity() {

        Paciente paciente = new Paciente(getId());
        paciente.setNombres(nombres);
        paciente.setApellidos(apellidos);
        paciente.setNumeroPaciente(numeroPaciente);
        paciente.setGenero(genero);
        paciente.setFechaNacimiento(LocalDate.parse(fechaNacimiento,dateFormat));

        if(valoresNormalesDTO != null)
            paciente.setValoresNormales(valoresNormalesDTO.convertToEntity());

        return paciente;
    }

    public Paciente convertToEntityBasic(){
        Paciente paciente = new Paciente(getId());

        return paciente;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Long getNumeroPaciente() {
        return numeroPaciente;
    }

    public void setNumeroPaciente(Long numeroPaciente) {
        this.numeroPaciente = numeroPaciente;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public ValoresNormalesDTO getValoresNormalesDTO() {
        return valoresNormalesDTO;
    }

    public void setValoresNormalesDTO(ValoresNormalesDTO valoresNormalesDTO) {
        this.valoresNormalesDTO = valoresNormalesDTO;
    }
}
