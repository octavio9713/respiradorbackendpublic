package unrn.edu.ar.iot.dto;

import unrn.edu.ar.iot.model.Paciente;
import unrn.edu.ar.iot.model.PeriodoDeAtencion;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PeriodoDeAtencionDTO extends AbstractDTO {

    private String fechaInicio;
    private String fechaFin;
    private List<SignosVitalesDTO> signosVitales = new ArrayList<>();

    private PacienteDTO pacienteDTO;

    private DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public PeriodoDeAtencionDTO() {
    }

    public PeriodoDeAtencionDTO(Long id) {
        this.id = id;
    }

    public PeriodoDeAtencionDTO(Long id, LocalDate fechaInicio, LocalDate fechaFin, Paciente paciente) {
        this.id = id;
        this.fechaInicio = fechaInicio.format(dateFormat);
        if (fechaFin != null)
            this.fechaFin = fechaFin.format(dateFormat);

        if(paciente != null)
            pacienteDTO = paciente.convertToDTO();
    }

    @Override
    public PeriodoDeAtencion convertToEntity() {

        PeriodoDeAtencion periodoDeAtencion = new PeriodoDeAtencion(getId());
        periodoDeAtencion.setFechaInicio(LocalDate.parse(fechaInicio,dateFormat));
        periodoDeAtencion.setFechaFin(LocalDate.parse(fechaFin,dateFormat));

        periodoDeAtencion.setPaciente(pacienteDTO.convertToEntityBasic());

        return periodoDeAtencion;
    }

    public PeriodoDeAtencion convertToAllEntity() {

        PeriodoDeAtencion periodoDeAtencion = this.convertToEntity();
        signosVitales.stream().forEach(signoVital -> periodoDeAtencion.addSignosVitales(signoVital.convertToEntity()));
        return periodoDeAtencion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public List<SignosVitalesDTO> getSignosVitales() {
        return signosVitales;
    }

    public void setSignosVitales(List<SignosVitalesDTO> signosVitales) {
        this.signosVitales = signosVitales;
    }

    public PacienteDTO getPacienteDTO() {
        return pacienteDTO;
    }

    public void setPacienteDTO(PacienteDTO pacienteDTO) {
        this.pacienteDTO = pacienteDTO;
    }
}
