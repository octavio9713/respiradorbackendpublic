package unrn.edu.ar.iot.dao.impl;

import unrn.edu.ar.iot.dao.UserDAO;
import unrn.edu.ar.iot.generic.GenericDaoJpaImpl;
import unrn.edu.ar.iot.model.security.User;

import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Named("userDao")
public class UserDAOImpl extends GenericDaoJpaImpl<User, Long> implements UserDAO, Serializable {

    private String queryGetUser = "select u from User u left join fetch u.fxs f left join fetch f.target t";

    public Predicate[] getSearchPredicates(Root<User> root, Map<String, String> filters) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();
        root.alias("entity");

        String description = filters.get("username");
        if (description != null && !"".equals(description)) {
            predicatesList.add(builder.like(root.<String>get("username"), '%' + description + '%'));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    @Override
    public User getEntityByName(String username) {

        StringBuffer queryByUsername = new StringBuffer(queryGetUser).
                append(" where u.username = :username and u.enabled = :enabled");
        Query query = this.entityManager.createQuery(queryByUsername.toString());

        query.setParameter("username", username);
        query.setParameter("enabled", true);

        return (User) query.getSingleResult();
    }

    public User getEntityByMail(String mail) {

        StringBuffer queryByUserMail = new StringBuffer(queryGetUser).
                append(" where u.email = :mail");
        Query query = this.entityManager.createQuery(queryByUserMail.toString());
        query.setParameter("mail", mail);

        return (User) query.getSingleResult();
    }

    @Override
    public User read(Long id) {
        StringBuffer querySB = new StringBuffer(queryGetUser);
        querySB.append(" where u.id = :id");
        Query query = this.entityManager.createQuery(querySB.toString());
        query.setParameter("id", id);
        return (User) query.getSingleResult();
    }

    public String queryEntity() {
        return this.queryGetUser;
    }

    @Override
    public List<User> findAll() {
        Query query = this.entityManager.createQuery(
                "select user from User user");
        return query.getResultList();
    }
}