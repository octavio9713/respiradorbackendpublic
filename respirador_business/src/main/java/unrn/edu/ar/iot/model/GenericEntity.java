package unrn.edu.ar.iot.model;

public interface GenericEntity<T> {

    T getId();

    void setId(T id);

}