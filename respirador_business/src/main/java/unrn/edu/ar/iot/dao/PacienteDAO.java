package unrn.edu.ar.iot.dao;

import unrn.edu.ar.iot.generic.GenericDao;
import unrn.edu.ar.iot.model.Paciente;

public interface PacienteDAO extends GenericDao<Paciente, Long> {

    Paciente save(Paciente survey);

    Paciente readByNumber(Long number);

}
