package unrn.edu.ar.iot.service.impl;

import org.springframework.transaction.annotation.Transactional;
import unrn.edu.ar.iot.dao.AlertaDAO;
import unrn.edu.ar.iot.dao.ValoresNormalesDAO;
import unrn.edu.ar.iot.model.ValoresNormales;
import unrn.edu.ar.iot.service.ValoresNormalesService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Named("valoresNormalesService")
public class ValoresNormalesServiceImpl extends GenericServiceImpl<ValoresNormales> implements ValoresNormalesService {

    @Inject
    ValoresNormalesDAO entityDAO;


    @Override
    protected ValoresNormalesDAO getEntityDAO() {
        return entityDAO;
    }

    @Override
    public List<ValoresNormales> getList(Integer page, Integer pagesize, Map<String, String> filters, String sortField, Boolean asc, boolean distinct) {
        return super.getList(page, pagesize, filters, sortField, asc, distinct);
    }

    @Override
    @Transactional
    public Long create(ValoresNormales entity) {
        return super.create(entity);
    }

    @Override
    @Transactional
    public ValoresNormales save(ValoresNormales entity) {
        return entityDAO.save(entity);
    }

    @Override
    @Transactional
    public Long update(ValoresNormales entity) {
        return super.update(entity);
    }

    @Override
    @Transactional
    public void delete(ValoresNormales entity) {
        super.delete(entity);
    }

    @Override
    @Transactional
    public ValoresNormales get(Long id) {
        return super.get(id);
    }

    @Override
    public List<ValoresNormales> getAll() {
        return new ArrayList<>(getEntityDAO().findAll());
    }
}
