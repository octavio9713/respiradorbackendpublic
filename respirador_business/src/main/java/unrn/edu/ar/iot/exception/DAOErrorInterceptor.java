package unrn.edu.ar.iot.exception;

import unrn.edu.ar.iot.logger.Log;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.springframework.core.annotation.Order;

@Aspect
@Order(1)
public class DAOErrorInterceptor {

    static @Log
    Logger LOG;

    @AfterThrowing(pointcut = "within(ar.edu.unrn.lia.service..*)", throwing = "ex")
    public void errorInterceptor(Exception ex) throws Exception {

        LOG.debug( "Error Message Interceptor started" );
        LOG.debug( "Error: "+ex.getMessage() );

        if (ex.getCause().getClass().getSimpleName().equals(ConstraintViolationException.class.getSimpleName())) {
            ConstraintViolationException error = (ConstraintViolationException) ex.getCause();
            switch (error.getErrorCode()) {
                case 1062:
                    throw new ConstraintViolationException("El registro ya existe en el sistema.", error.getSQLException(), null);
                case 1451:
                    throw new ConstraintViolationException("No se puede eliminar el registro debido a que est� relacionado con otros datos del sistema.", error.getSQLException(), null);
            }
        }


    }

}