package unrn.edu.ar.iot.dao.impl;

import unrn.edu.ar.iot.dao.PacienteDAO;
import unrn.edu.ar.iot.generic.GenericDaoJpaImpl;
import unrn.edu.ar.iot.model.Paciente;

import javax.inject.Named;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Named("pacienteDao")
public class PacienteDAOImpl extends GenericDaoJpaImpl<Paciente, Long> implements PacienteDAO, Serializable {

    private String queryString = "select p from Paciente p ";
    //"LEFT JOIN FETCH p.periodosDeAtencion pa " + "LEFT JOIN FETCH pa.signosVitales sv ";

    public Predicate[] getSearchPredicates(Root<Paciente> root, Map<String, String> filters) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();
        root.alias("entity");


        String userId = filters.get("createdBy");
        if (userId != null && !"".equals(userId)) {
            predicatesList.add(builder
                    .equal(root.get("createdBy").<Long>get("id"), userId));

        }

        String  userName = filters.get("createdBy.username");
        if (userName != null && !userName.isEmpty()) {
            predicatesList.add(builder.like(root.get("createdBy").<String>get("username"), userName));
        }


        String title = filters.get("title");
        if (title != null && !"".equals(title)) {
            predicatesList.add(builder.like(root.<String>get("title"), '%' + title + '%'));
        }

        String typeDate = filters.get("typeDate");
        String today = filters.get("fecha");
        if (today != null && !"".equals(today)) {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
            Date datenow = null;
            datenow = new Date();
            try {
                String requiredDate = df.format(datenow);
                date = formatter.parse(requiredDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            predicatesList.add(builder.greaterThanOrEqualTo(root.<Date>get("createDate"), date));
        }

        String surveyType = filters.get("surveyType");
        if (surveyType != null && !"".equals(surveyType)) {
            predicatesList.add(
                    builder.equal(root.get("surveyType").get("id"),
                            Long.valueOf(surveyType)));
        }

        //TODO:REVISAR
       /* String createdBy = filters.get("createdBy");
        if (createdBy != null && !"".equals(createdBy)) {
            predicatesList.add(
                    builder.equal(root.get("createdBy").get("id"),
                            Long.valueOf(createdBy)));
        }*/

        String fechaInicio = filters.get("fecha_inicio");
        String fechaFin = filters.get("fecha_fin");
        if (fechaInicio != null && !"".equals(fechaInicio) && fechaFin != null && !"".equals(fechaFin)) {

            Date fDesde = null;
            Date fHasta = null;
            try {
                fDesde = formatter.parse(fechaInicio);
                fHasta = formatter.parse(fechaFin);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Predicate dateBetween = builder.between(builder.function("date", Date.class,
                    root.<Date>get("createDate")), fDesde, fHasta);
            predicatesList.add(dateBetween);

        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    /*
    @Override
    public List<Survey> findByQuery(String query, String propertyFilter, String orderDirection) {
        return super.findByQuery(query, propertyFilter, orderDirection);
    }

    @Override
    public List<Survey> listwithPag(Predicate[] where, Integer page, Integer pagesize, String sortField, Boolean asc, boolean distinct) {
        return super.listwithPag(where, page, pagesize, sortField, asc, distinct);
    }

    @Override
    protected Order orderByQuery(String sortField, Boolean asc, CriteriaBuilder builder, Root<Survey> root) {

        if (sortField.equals("number")) {

            Expression number = root.get("number");
            Expression numberCast = builder.function("ABS", Integer.class, number);

            if (asc)
                return builder.asc(numberCast);
            else
                return builder.desc(numberCast);

        }
        return super.orderByQuery(sortField, asc, builder, root);
    }

    @Override
    public Long count(Predicate[] where) {
        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        countCriteria.distinct(true);
        Root<Survey> root = countCriteria.from(Survey.class);
        root.alias("entity");

        countCriteria = countCriteria.select(builder.count(root)).where(where);
        return this.entityManager.createQuery(countCriteria).getSingleResult();
    }
    */

    @Override
    public Paciente read(Long id) {
        StringBuffer querySB = new StringBuffer(queryString);
        querySB.append(" WHERE p.id= :id");
        Query query = this.entityManager.createQuery(querySB.toString());
        query.setParameter("id", id);
        return (Paciente) query.getSingleResult();
    }

    @Override
    public Paciente readByNumber(Long number) {
        StringBuffer querySB = new StringBuffer(queryString);
        querySB.append(" WHERE p.numeroPaciente= :number");
        Query query = this.entityManager.createQuery(querySB.toString());
        query.setParameter("number", number);
        return (Paciente) query.getSingleResult();
    }

    @Override
    public Paciente save(Paciente paciente) {
        this.entityManager.persist(paciente);
        return paciente;
    }
}
