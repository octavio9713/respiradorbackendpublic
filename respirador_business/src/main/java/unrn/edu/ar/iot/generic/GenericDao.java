package unrn.edu.ar.iot.generic;

import javax.persistence.EntityManager;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface GenericDao<T, PK extends Serializable> {
	EntityManager getEntityManager();

	Long create(T t);

	T read(PK id);

	T update(T t);

	void delete(T t);

	void delete(PK id);

	List<T> findAll();

	Long count(Predicate[] where);

	Root<T> rootCount();

	Predicate[] getSearchPredicates(Root<T> root,
                                    Map<String, String> filters);

	List<T> listwithPag(Predicate[] where, Integer page,
                        Integer pagesize, String sortField, Boolean asc, boolean distinct);

	List<T> listwithPag(Predicate[] where, Integer page,
                        Integer pagesize, Map<String, Boolean> sorts);

	List<T> findByQuery(String query, String propertyFilter,
                        String orderDirection);

}