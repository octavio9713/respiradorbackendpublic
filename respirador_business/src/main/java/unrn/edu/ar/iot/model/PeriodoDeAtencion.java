package unrn.edu.ar.iot.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import unrn.edu.ar.iot.dto.PeriodoDeAtencionDTO;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "periodo_de_atencion")
public class PeriodoDeAtencion extends AbstractEntity {

    @Column(name = "fecha_inicio")
    private LocalDate fechaInicio;
    @Column(name = "fecha_fin")
    private LocalDate fechaFin;

    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "id_paciente")
    private Paciente paciente;

    @OneToMany(
            mappedBy = "periodoDeAtencion",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Fetch(FetchMode.JOIN)
    private Set<SignosVitales> signosVitales = new HashSet<>();

    public PeriodoDeAtencion() {
    }

    public PeriodoDeAtencion(Long id) {
        this.setId(id);
    }

    @Transient
    public PeriodoDeAtencionDTO convertToDTO() {
        PeriodoDeAtencionDTO dto = new PeriodoDeAtencionDTO(this.getId(), this.fechaInicio, this.fechaFin, null);

        return dto;
    }

    @Transient
    public PeriodoDeAtencionDTO convertToAllDTO() {

        PeriodoDeAtencionDTO dto = this.convertToDTO();
        dto.setSignosVitales(this.signosVitales.stream().map(SignosVitales::convertToAllDTO).collect(Collectors.toList()));

        return dto;
    }

    @Transient
    public void addSignosVitales(SignosVitales signosVitales) {
        signosVitales.setPeriodoDeAtencion(this);
        this.signosVitales.add(signosVitales);
    }

    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public LocalDate getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    public List<SignosVitales> getSignosVitales() {
        return signosVitales.stream().collect(Collectors.toList());
    }

    public Set<SignosVitales> setSignosVitales() {
        return signosVitales;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
}
