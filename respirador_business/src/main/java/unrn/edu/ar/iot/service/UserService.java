package unrn.edu.ar.iot.service;

import unrn.edu.ar.iot.model.security.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends GenericService<User>, UserDetailsService {

    User findByMail(String mail);

    void cambiarClave(User user);

    User validationByUsername(String username);

    User findByUsername(String username);

    boolean isUserExist(User user);

    User getLoggedUser();

    List<User> findAll();

    boolean checkPassword(String newPassword);

    String idLoggedUser();

}