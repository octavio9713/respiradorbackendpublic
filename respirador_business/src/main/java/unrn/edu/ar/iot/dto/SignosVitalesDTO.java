package unrn.edu.ar.iot.dto;

import com.google.api.client.util.DateTime;
import unrn.edu.ar.iot.model.PeriodoDeAtencion;
import unrn.edu.ar.iot.model.SignosVitales;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class SignosVitalesDTO extends AbstractDTO {

    private String fechaObtencion;
    private String horaObtencion;
    private int presionSistolica;
    private int presionDiastolica;
    private int pulso;
    private int oxigenoEnSangre;
    private int respiracion;
    private Float temperatura;

    private List<AlertaDTO> alertas;
    private PeriodoDeAtencionDTO periodoDeAtencionDTO;

    private DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("hh:mm:ss");

    public SignosVitalesDTO() {
    }

    public SignosVitalesDTO(Long id) {
        this.id = id;
    }

    public SignosVitalesDTO(Long id, int presionSistolica, int presionDiastolica, int pulso, int oxigenoEnSangre, int respiracion, Float temperatura, LocalDateTime fechaObtencion, PeriodoDeAtencion periodoDeAtencion) {
        this.id = id;
        this.presionSistolica = presionSistolica;
        this.presionDiastolica = presionDiastolica;
        this.pulso = pulso;
        this.oxigenoEnSangre = oxigenoEnSangre;
        this.respiracion = respiracion;
        this.temperatura = temperatura;
        this.fechaObtencion = fechaObtencion.format(dateFormat);
        this.horaObtencion = fechaObtencion.format(timeFormat);

        if (periodoDeAtencion != null)
            periodoDeAtencionDTO = periodoDeAtencion.convertToDTO();
    }

    @Override
    public SignosVitales convertToEntity() {

        SignosVitales signosVitales = new SignosVitales(getId());
        signosVitales.setPresionSistolica(this.presionSistolica);
        signosVitales.setPresionDiastolica(this.presionDiastolica);
        signosVitales.setPulso(this.pulso);
        signosVitales.setOxigenoEnSangre(this.oxigenoEnSangre);
        signosVitales.setRespiracion(respiracion);
        signosVitales.setTemperatura(temperatura);
        signosVitales.setFechaObtencion(
                LocalDateTime.of(
                        LocalDate.parse(fechaObtencion, dateFormat),
                        LocalTime.parse(horaObtencion)));

        return signosVitales;
    }

    public int getPresionSistolica() {
        return presionSistolica;
    }

    public void setPresionSistolica(int presionSistolica) {
        this.presionSistolica = presionSistolica;
    }

    public int getPresionDiastolica() {
        return presionDiastolica;
    }

    public void setPresionDiastolica(int presionDiastolica) {
        this.presionDiastolica = presionDiastolica;
    }

    public int getPulso() {
        return pulso;
    }

    public void setPulso(int pulso) {
        this.pulso = pulso;
    }

    public int getOxigenoEnSangre() {
        return oxigenoEnSangre;
    }

    public void setOxigenoEnSangre(int oxigenoEnSangre) {
        this.oxigenoEnSangre = oxigenoEnSangre;
    }

    public int getRespiracion() {
        return respiracion;
    }

    public void setRespiracion(int respiracion) {
        this.respiracion = respiracion;
    }

    public Float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Float temperatura) {
        this.temperatura = temperatura;
    }

    public String getFechaObtencion() {
        return fechaObtencion;
    }

    public void setFechaObtencion(String fechaObtencion) {
        this.fechaObtencion = fechaObtencion;
    }

    public String getHoraObtencion() {
        return horaObtencion;
    }

    public void setHoraObtencion(String horaObtencion) {
        this.horaObtencion = horaObtencion;
    }

    public List<AlertaDTO> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<AlertaDTO> alertas) {
        this.alertas = alertas;
    }
}
