package unrn.edu.ar.iot.service;

import unrn.edu.ar.iot.model.Alerta;

import java.util.List;

public interface AlertaService extends GenericService<Alerta> {

    Alerta save(Alerta entity);

    List<Alerta> getAll();

}
