package unrn.edu.ar.iot.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextConsumer implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public final void setApplicationContext(ApplicationContext ac)
            throws BeansException {
        this.applicationContext = ac;


    }

    public static final <T> T getBean(Class<T> type) {
        return applicationContext.getBean(type);
    }
}
