package unrn.edu.ar.iot.service.impl;

import unrn.edu.ar.iot.dao.PacienteDAO;
import unrn.edu.ar.iot.model.Paciente;
import org.springframework.transaction.annotation.Transactional;
import unrn.edu.ar.iot.service.PacienteService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Named("pacienteService")
public class PacienteServiceImpl extends GenericServiceImpl<Paciente> implements PacienteService {

    @Inject
    PacienteDAO entityDAO;


    @Override
    protected PacienteDAO getEntityDAO() {
        return entityDAO;
    }

    @Override
    public List<Paciente> getList(Integer page, Integer pagesize, Map<String, String> filters, String sortField, Boolean asc, boolean distinct) {
        return super.getList(page, pagesize, filters, sortField, asc, distinct);
    }

    @Override
    @Transactional
    public Long create(Paciente entity) {
        return super.create(entity);
    }

    @Override
    @Transactional
    public Paciente save(Paciente entity) {
        return entityDAO.save(entity);
    }

    @Override
    @Transactional
    public Long update(Paciente entity) {
        return super.update(entity);
    }

    @Override
    @Transactional
    public void delete(Paciente entity) {
        super.delete(entity);
    }

    @Override
    @Transactional
    public Paciente get(Long id) {
        return super.get(id);
    }

    @Override
    public List<Paciente> getAll() {
        return new ArrayList<>(getEntityDAO().findAll());
    }

    @Override
    public Paciente getByPatientNumber(long number) {
        return getEntityDAO().readByNumber(number);
    }
}
