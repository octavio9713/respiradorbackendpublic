package unrn.edu.ar.iot.model;

import unrn.edu.ar.iot.dto.AlertaDTO;

import javax.persistence.*;

@Entity
@Table(name = "alerta")
public class Alerta extends AbstractEntity {

    private String signo;
    private String motivo;

    @Column(name = "mensaje_de_alerta")
    private String mensajeDeAlerta;

    public Alerta() {
    }

    public Alerta(Long id) {
        this.setId(id);
    }

    @Transient
    public AlertaDTO convertToDTO() {
        AlertaDTO dto = new AlertaDTO(this.getId(), this.getSigno(), this.getMotivo(), this.getMensajeDeAlerta());

        return dto;
    }

    public String getSigno() {
        return signo;
    }

    public void setSigno(String signo) {
        this.signo = signo;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getMensajeDeAlerta() {
        return mensajeDeAlerta;
    }

    public void setMensajeDeAlerta(String mensajeDeAlerta) {
        this.mensajeDeAlerta = mensajeDeAlerta;
    }
}
