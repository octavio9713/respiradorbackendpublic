package unrn.edu.ar.iot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by juan on 02/02/17.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UnAuthorizedException extends SecurityException {

    public UnAuthorizedException() {
        super("exceptions.unauthorized");
    }

    public UnAuthorizedException(String detail) {
        super(detail);
    }
}
