----------- 29/06/19 ----------

ALTER TABLE `paciente` ADD `nombres` VARCHAR(100) NOT NULL AFTER `id`, ADD `apellidos` VARCHAR(100) NOT NULL AFTER `nombres`;

RENAME TABLE `respirador`.`signas_vitales_alerta` TO `respirador`.`signos_vitales_alertas`;

ALTER TABLE `alerta` DROP INDEX `mensaje_de_alerta_UNIQUE`;

ALTER TABLE `alerta` CHANGE `mensaje_de_alerta` `mensaje_de_alerta` VARCHAR(99) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `signos_vitales` CHANGE `presion_sistolica` `presion_sistolica` INT(3) NULL, CHANGE `presion_diastolica` `presion_diastolica` INT(3) NULL, CHANGE `pulso` `pulso` INT(3) NULL, CHANGE `oxigeno_en_sangre` `oxigeno_en_sangre` FLOAT NULL, CHANGE `respiracion` `respiracion` INT(11) NULL, CHANGE `temperatura` `temperatura` FLOAT NULL;
