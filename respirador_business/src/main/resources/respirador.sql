-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-06-2020 a las 22:22:41
-- Versión del servidor: 5.7.19
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `respirador`
--
CREATE DATABASE IF NOT EXISTS `respirador` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `respirador`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afeccion`
--

DROP TABLE IF EXISTS `afeccion`;
CREATE TABLE IF NOT EXISTS `afeccion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alerta`
--

DROP TABLE IF EXISTS `alerta`;
CREATE TABLE IF NOT EXISTS `alerta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `signo` varchar(45) NOT NULL,
  `motivo` varchar(45) NOT NULL,
  `mensaje_de_alerta` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mensaje_de_alerta_UNIQUE` (`mensaje_de_alerta`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alerta`
--

INSERT INTO `alerta` (`id`, `signo`, `motivo`, `mensaje_de_alerta`) VALUES
(1, 'Pulso', 'Supera El Maximo', 'Mensaje de Alerta 1'),
(2, 'Pulso', 'No Alcanza el Minimo', 'Mensaje de Alerta 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfermero`
--

DROP TABLE IF EXISTS `enfermero`;
CREATE TABLE IF NOT EXISTS `enfermero` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `legajo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

DROP TABLE IF EXISTS `paciente`;
CREATE TABLE IF NOT EXISTS `paciente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_paciente` int(11) NOT NULL,
  `genero` varchar(10) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `id_valores_normales` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `numero_paciente_UNIQUE` (`numero_paciente`),
  UNIQUE KEY `id_valores_normales_UNIQUE` (`id_valores_normales`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`id`, `numero_paciente`, `genero`, `fecha_nacimiento`, `id_valores_normales`) VALUES
(1, 1, 'Femenino', '2014-08-05', 1),
(2, 2, 'Masculino', '1997-10-07', NULL),
(3, 3, 'Masculino', '2004-01-26', NULL),
(6, 4, 'Masculino', '1997-10-07', 2),
(7, 5, 'Masculino', '1997-10-08', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodo_de_atencion`
--

DROP TABLE IF EXISTS `periodo_de_atencion`;
CREATE TABLE IF NOT EXISTS `periodo_de_atencion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date DEFAULT NULL,
  `id_paciente` int(11) NOT NULL,
  `id_respirador` int(11) NOT NULL,
  PRIMARY KEY (`id`,`fecha_inicio`,`id_paciente`),
  KEY `periodo_paciente_fk_idx` (`id_paciente`),
  KEY `periodo_respirador_idx` (`id_respirador`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `periodo_de_atencion`
--

INSERT INTO `periodo_de_atencion` (`id`, `fecha_inicio`, `fecha_fin`, `id_paciente`, `id_respirador`) VALUES
(2, '2020-06-01', '2020-06-17', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `problema`
--

DROP TABLE IF EXISTS `problema`;
CREATE TABLE IF NOT EXISTS `problema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_respirador` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `problema_respirador_fk_idx` (`id_respirador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respirador`
--

DROP TABLE IF EXISTS `respirador`;
CREATE TABLE IF NOT EXISTS `respirador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio_actividad` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `respirador`
--

INSERT INTO `respirador` (`id`, `fecha_inicio_actividad`) VALUES
(1, '2020-06-01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `signas_vitales_alerta`
--

DROP TABLE IF EXISTS `signas_vitales_alerta`;
CREATE TABLE IF NOT EXISTS `signas_vitales_alerta` (
  `id_signos_vitales` int(11) NOT NULL,
  `id_alerta` int(11) NOT NULL,
  PRIMARY KEY (`id_signos_vitales`,`id_alerta`),
  KEY `alerta_signos_vitales_fk_idx` (`id_alerta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `signos_vitales`
--

DROP TABLE IF EXISTS `signos_vitales`;
CREATE TABLE IF NOT EXISTS `signos_vitales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_periodo` int(11) NOT NULL,
  `fecha_obtencion` datetime NOT NULL,
  `presion_sistolica` int(3) NOT NULL,
  `presion_diastolica` int(3) NOT NULL,
  `pulso` int(3) NOT NULL,
  `oxigeno_en_sangre` float NOT NULL,
  `respiracion` int(11) NOT NULL,
  `temperatura` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `signos_periodo_idx` (`id_periodo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `signos_vitales`
--

INSERT INTO `signos_vitales` (`id`, `id_periodo`, `fecha_obtencion`, `presion_sistolica`, `presion_diastolica`, `pulso`, `oxigeno_en_sangre`, `respiracion`, `temperatura`) VALUES
(1, 2, '2020-06-16 03:50:00', 80, 120, 101, 99, 15, 35),
(2, 2, '2020-06-16 04:00:00', 80, 120, 102, 99, 15, 36.5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `password_UNIQUE` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `created`, `email`, `username`, `password`, `enabled`) VALUES
(1, NULL, 'alehuaiquilican@gmail.com', 'huaiky', '$2a$10$yaPDsdDNlO9oPej/VMULS.DWJdqWJEpmeznpXa9v6yKCpQ1pS6XJW', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores_normales`
--

DROP TABLE IF EXISTS `valores_normales`;
CREATE TABLE IF NOT EXISTS `valores_normales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `presion_sistolica_minima` int(11) NOT NULL,
  `presion_sistolica_maxima` int(11) NOT NULL,
  `presion_diastolica_minima` int(11) NOT NULL,
  `presion_diastolica_maxima` int(11) NOT NULL,
  `pulso_minimo` int(11) NOT NULL,
  `pulso_maximo` int(11) NOT NULL,
  `oxigeno_en_sangre_minimo` int(11) NOT NULL,
  `oxigeno_en_sangre_maximo` int(11) NOT NULL,
  `respiracion_minima` int(11) NOT NULL,
  `respiracion_maxima` int(11) NOT NULL,
  `temperatura_minima` float NOT NULL,
  `temperatura_maxima` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `valores_normales`
--

INSERT INTO `valores_normales` (`id`, `presion_sistolica_minima`, `presion_sistolica_maxima`, `presion_diastolica_minima`, `presion_diastolica_maxima`, `pulso_minimo`, `pulso_maximo`, `oxigeno_en_sangre_minimo`, `oxigeno_en_sangre_maximo`, `respiracion_minima`, `respiracion_maxima`, `temperatura_minima`, `temperatura_maxima`) VALUES
(1, 80, 85, 115, 120, 90, 110, 98, 99, 0, 0, 0, 0),
(2, 80, 85, 115, 120, 90, 110, 98, 99, 0, 0, 0, 0);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `paciente_valores_normales_fk` FOREIGN KEY (`id_valores_normales`) REFERENCES `valores_normales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `periodo_de_atencion`
--
ALTER TABLE `periodo_de_atencion`
  ADD CONSTRAINT `periodo_paciente_fk` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `periodo_respirador` FOREIGN KEY (`id_respirador`) REFERENCES `respirador` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `problema`
--
ALTER TABLE `problema`
  ADD CONSTRAINT `problema_respirador_fk` FOREIGN KEY (`id_respirador`) REFERENCES `respirador` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `signas_vitales_alerta`
--
ALTER TABLE `signas_vitales_alerta`
  ADD CONSTRAINT `alerta_signos_vitales_fk` FOREIGN KEY (`id_alerta`) REFERENCES `alerta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `signos_vitales_alerta_fk` FOREIGN KEY (`id_signos_vitales`) REFERENCES `signos_vitales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `signos_vitales`
--
ALTER TABLE `signos_vitales`
  ADD CONSTRAINT `signos_periodo` FOREIGN KEY (`id_periodo`) REFERENCES `periodo_de_atencion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
