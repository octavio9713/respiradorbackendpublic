package unrn.edu.ar.iot.mvc;

import org.springframework.http.MediaType;
import org.springframework.http.MediaType;
import unrn.edu.ar.iot.dto.AlertaDTO;
import unrn.edu.ar.iot.dto.PacienteDTO;
import unrn.edu.ar.iot.dto.PeriodoDeAtencionDTO;
import unrn.edu.ar.iot.dto.ValoresNormalesDTO;
import unrn.edu.ar.iot.model.Alerta;
import unrn.edu.ar.iot.model.Paciente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import unrn.edu.ar.iot.model.PeriodoDeAtencion;
import unrn.edu.ar.iot.service.AlertaService;
import unrn.edu.ar.iot.service.PacienteService;
import unrn.edu.ar.iot.service.PeriodoDeAtencionService;
import unrn.edu.ar.iot.service.ValoresNormalesService;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
public class PacienteController {

    final static Logger LOG = LoggerFactory.getLogger(PacienteController.class);

    private final String orderSurveyType = "name";
    private final String orderSurvey = "id";

    @Inject
    private PacienteService pacienteService;

    @Inject
    private AlertaService alertaService;

    @Inject
    private PeriodoDeAtencionService periodoDeAtencionService;

    @Inject
    private ValoresNormalesService valoresNormalesService;

    /*Crea una nueva alerta*/
    @RequestMapping(value = "/api/alerta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody AlertaDTO entity) {
        alertaService.create(entity.convertToEntity());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /*Actualiza una alerta*/
    @RequestMapping(value = "/api/alerta", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateAlerta(@RequestBody AlertaDTO entity) {
        alertaService.update(entity.convertToEntity());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*Retorna las alertas registradas en el sistema*/
    @GetMapping("/api/alertas")
    public ResponseEntity<List<AlertaDTO>> getAllAlertas() {
        List<Alerta> all = alertaService.getAll();
        List<AlertaDTO> allDTO = all.stream().map(Alerta::convertToDTO).collect(Collectors.toList());
        return new ResponseEntity<>(allDTO, HttpStatus.OK);
    }

    /*Crea un nuevo paciente*/
    @RequestMapping(value = "/api/paciente", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody PacienteDTO entity) {
        pacienteService.create(entity.convertToEntity());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /*Retorna los pacientes registrados en el sistema*/
    @GetMapping("/api/pacientes")
    public ResponseEntity<List<PacienteDTO>> getAllReasons() {
        List<Paciente> all = pacienteService.getAll();
        List<PacienteDTO> allDTO = all.stream().map(Paciente::convertToDTO).collect(Collectors.toList());
        return new ResponseEntity<>(allDTO, HttpStatus.OK);
    }

    /*Retorna los pacientes registrados en el sistema*/
    @GetMapping("/api/paciente/{number}")
    public ResponseEntity<PacienteDTO> getAPatient(@PathVariable Long number) {
        Paciente patient = pacienteService.getByPatientNumber(number);
        PacienteDTO DTO = patient.convertToDTO();
        return new ResponseEntity(DTO, HttpStatus.OK);
    }

    /*Retorna los periodos de atencion registrados para el paciente segun el numero del paciente*/
    @GetMapping("/api/paciente/periodos/{number}")
    public ResponseEntity<List<PeriodoDeAtencionDTO>> getPeriodosDeAtencionByPacienteNumber(@PathVariable Long number) {
        List<PeriodoDeAtencion> all = periodoDeAtencionService.getByNumberPaciente(number);
        List<PeriodoDeAtencionDTO> allDTO = all.stream().map(PeriodoDeAtencion::convertToAllDTO).collect(Collectors.toList());
        return new ResponseEntity<>(allDTO, HttpStatus.OK);
    }


    /*Crea un nuevo periodo de atencion*/
    @RequestMapping(value = "/api/periodo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createPeriodo(@RequestBody PeriodoDeAtencionDTO entity) {
        periodoDeAtencionService.create(entity.convertToEntity());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /*Actualiza un periodo de atencion*/
    @RequestMapping(value = "/api/periodo", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updatePeriodo(@RequestBody PeriodoDeAtencionDTO entity) {
        periodoDeAtencionService.update(entity.convertToAllEntity());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*Actualiza valores normales*/
    @RequestMapping(value = "/api/valores-normales", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateValoresNormales(@RequestBody ValoresNormalesDTO entity) {
        valoresNormalesService.update(entity.convertToEntity());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
