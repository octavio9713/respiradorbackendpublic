package unrn.edu.ar.iot.security.jwt;

import unrn.edu.ar.iot.config.ParamValue;
import unrn.edu.ar.iot.seguridad.AuthenticationService;
import unrn.edu.ar.iot.seguridad.JwtTokenUtil;
import unrn.edu.ar.iot.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Inject
    AuthenticationService authenticationService;

    @Inject
    private UserService userService;

    @Inject
    private JwtTokenUtil jwtTokenUtil;

    @ParamValue(key = "jwt.header")
    private String tokenHeader;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        try {
            User credenciales = new ObjectMapper().readValue(request.getInputStream(), User.class);

            return authenticationService.authenticate(credenciales.getUsername(), credenciales.getPassword());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {


        String token = jwtTokenUtil.generateToken((unrn.edu.ar.iot.model.security.User) auth.getPrincipal());

        response.addHeader(tokenHeader, ConstantsJWTConfig.TOKEN_BEARER_PREFIX + " " + token);
    }

}
