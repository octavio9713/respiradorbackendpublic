package unrn.edu.ar.iot;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.ArrayList;
import java.util.List;

/**
 * RepositoryRestMvcAutoConfiguration excluye los servicios rest que habilita por defecto.
 *
 */
@SpringBootApplication(exclude = RepositoryRestMvcAutoConfiguration.class)
@CrossOrigin(origins = "http://localhost:4200")
@EnableCaching
@EnableTransactionManagement
public class ServerApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
		SpringApplication.run(ServerApplication.class, args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(ServerApplication.class);
	}
    /*
        @Component
        static class Runner implements CommandLineRunner {
            @Autowired
            private ParameterRepository userRepository;
            public void run(String... args) throws Exception {
                log.info(".... Fetching user details");
                log.info("ParamValue 001 -->" + userRepository.findByKey("001"));
                log.info("ParamValue 001 -->" + userRepository.findByKey("001"));
                log.info("ParamValue 001 -->" + userRepository.findByKey("001"));
                log.info("ParamValue 002 -->" + userRepository.findByKey("002"));
                log.info("ParamValue 002 -->" + userRepository.findByKey("002"));
                log.info("ParamValue 002 -->" + userRepository.findByKey("002"));
            }
        }
        @Bean
        public CacheManager jdkCacheManager() {
            return new ConcurrentMapCacheManager("parameters");
        }
    */
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        List<Cache> caches = new ArrayList<>();
        caches.add(new ConcurrentMapCache("pacientes"));
        cacheManager.setCaches(caches);
        return cacheManager;
    }

}
