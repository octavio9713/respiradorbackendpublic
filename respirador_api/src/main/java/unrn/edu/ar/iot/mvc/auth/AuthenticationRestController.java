package unrn.edu.ar.iot.mvc.auth;

import unrn.edu.ar.iot.config.ParamValue;
import unrn.edu.ar.iot.dto.UserDTO;
import unrn.edu.ar.iot.model.security.User;
import unrn.edu.ar.iot.security.jwt.JwtAuthenticationRequest;
import unrn.edu.ar.iot.seguridad.AuthenticationService;
import unrn.edu.ar.iot.seguridad.JwtAuthenticationResponse;
import unrn.edu.ar.iot.seguridad.JwtTokenUtil;
import unrn.edu.ar.iot.seguridad.JwtUser;
import unrn.edu.ar.iot.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin
@RequestMapping(value = "/api")
public class AuthenticationRestController {

    @ParamValue(key = "jwt.header")
    private String tokenHeader;

    @Inject
    private AuthenticationService authenticationService;

    @Inject
    private JwtTokenUtil jwtTokenUtil;
    @Inject
    private UserService userService;

    @RequestMapping(value = "auth", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest request) throws AuthenticationException {

        final String token = authenticationService.authenticateWithJWT(request.getUsername(), request.getPassword());

        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
    }

    @RequestMapping(value = "user", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> getAuthenticated(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        User user = (User) userService.loadUserByUsername(username);
        return new ResponseEntity<>(user.convertToDTO(), HttpStatus.OK);
    }

    @RequestMapping(value = "refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token)) {
            String refreshedToken = jwtTokenUtil.refreshToken(user, token);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

}
