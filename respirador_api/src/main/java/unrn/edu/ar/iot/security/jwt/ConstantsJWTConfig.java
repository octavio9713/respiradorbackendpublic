package unrn.edu.ar.iot.security.jwt;

public class ConstantsJWTConfig {

	// Spring Security

	public static final String AUTH = "/api/auth";
	public static final String REST = "/api";
	public static final String HEADER_AUTHORIZACION_KEY = "Authorization";
	public static final String TOKEN_BEARER_PREFIX = "Bearer ";

	// JWT

	public static final String ISSUER_INFO = "https://www.autentia.com/";
	public static final String SUPER_SECRET_KEY = "1234";
	public static final long TOKEN_EXPIRATION_TIME = 864_000_000; // 10 day

}
