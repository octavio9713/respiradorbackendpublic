package unrn.edu.ar.iot.seguridad;

import unrn.edu.ar.iot.model.security.User;
import unrn.edu.ar.iot.service.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named("authenticationService")
public class AuthenticationServiceImpl implements AuthenticationService, Serializable {

    private static final long serialVersionUID = 5371998321050759039L;

    @Inject
    private AuthenticationManager authenticationManager;

    @Inject
    private UserService userService;

    @Inject
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public boolean login(String username, String password)
            throws AuthenticationException, UsernameNotFoundException {

        Authentication authenticate = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(username,
                        password));
        if (authenticate.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authenticate);
            return true;
        }

        return false;
    }

    @Override
    public Authentication authenticate(String username, String password) {

        Authentication authenticate = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(username,
                        password));
        return authenticate;
    }

    @Override
    public Authentication authenticate(String username, String password, List<GrantedAuthority> authorities) {

        Authentication authenticate = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(username,
                        password, authorities));
        return authenticate;
    }

    @Override
    public String authenticateWithJWT(String username, String password) {
       // final Authentication authentication = this.authenticate(username, password);
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        username,
                        password
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        //final UserDetails userDetails = userService.loadUserByUsername(username);
        UserDetails userDetails = (User) authentication.getPrincipal();
        return jwtTokenUtil.generateToken(userDetails);
    }

    @Override
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        SecurityContextHolder.clearContext();
    }

}
