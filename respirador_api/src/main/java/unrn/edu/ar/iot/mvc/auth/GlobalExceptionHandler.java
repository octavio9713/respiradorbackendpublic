package unrn.edu.ar.iot.mvc.auth;

import unrn.edu.ar.iot.exception.UnAuthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<?> handleNotAuthenticatedException(HttpServletRequest request, Exception ex){
        logger.info("Credenciales invalidas 1");
        ErrorResponse errorResponse = new ErrorResponse("Usuario y/o Password no son correctos");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity<>(errorResponse, headers, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(InternalAuthenticationServiceException.class)
    public ResponseEntity<?> handleNotAuthenticated2Exception(HttpServletRequest request, Exception ex){
        logger.info("Credenciales invalidas 2");
        ErrorResponse errorResponse = new ErrorResponse("Usuario y/o Password no son correctos");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity<>(errorResponse, headers, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(UnAuthorizedException.class)
    public ResponseEntity<?> handleUnAuthorizedException(HttpServletRequest request, Exception ex){
        logger.info("El usuario no posee los permisos necesarios");
        ErrorResponse errorResponse = new ErrorResponse("El Usuario no posee los permisos necesarios");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity<>(errorResponse, headers, HttpStatus.FORBIDDEN);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleErrorException(HttpServletRequest request, Exception ex){
        logger.info("Error Interno: " + ex.getMessage());
        ErrorResponse errorResponse = new ErrorResponse("Error interno en el  servidor, intente nuevamente");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");

        return new ResponseEntity<>(errorResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private class ErrorResponse {
        private String message;

        ErrorResponse(String message){
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}