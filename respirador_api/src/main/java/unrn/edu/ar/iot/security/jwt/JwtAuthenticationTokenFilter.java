package unrn.edu.ar.iot.security.jwt;

import unrn.edu.ar.iot.config.ParamValue;
import unrn.edu.ar.iot.model.security.User;
import unrn.edu.ar.iot.seguridad.AuthenticationService;
import unrn.edu.ar.iot.seguridad.JwtTokenUtil;
import unrn.edu.ar.iot.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @ParamValue(key = "jwt.header")
    private String tokenHeader = "Authorization";
    @Inject
    private AuthenticationManager authenticationManager;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authToken = request.getHeader(this.tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(authToken);

        String url = request.getRequestURI();
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            User user = (User) userService.loadUserByUsername(username);

           // List<GrantedAuthority> authorities = JwtUserFactory.mapToGrantedAuthorities(user.getFxs());

           UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                    user,
                    null,
                   null);


          //  authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        chain.doFilter(request, response);
    }

    public AuthenticationManager getAuthenticationManager() {
        return authenticationManager;
    }

    public UserService getUserService() {
        return userService;
    }

    public JwtTokenUtil getJwtTokenUtil() {
        return jwtTokenUtil;
    }

    public String getTokenHeader() {
        return tokenHeader;
    }
}