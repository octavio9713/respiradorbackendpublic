package unrn.edu.ar.iot.seguridad;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

public interface AuthenticationService {
	
	boolean login(String username, String password);

	Authentication authenticate(String username, String password);

	Authentication authenticate(String username, String password, List<GrantedAuthority> authorities);

	void logout();

	String authenticateWithJWT(String username, String password);
}
