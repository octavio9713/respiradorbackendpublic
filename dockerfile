FROM openjdk:11-slim AS builder
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY . .
RUN ./gradlew bootjar

FROM openjdk:11-slim AS docker
LABEL maintainer="olinares@unrn.edu.ar"
LABEL "unrn.edu.ar"="UNRN"
LABEL "service"="Backend for respirator"
COPY --from=builder /usr/app/build/libs/*.jar app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=docker","-jar","app.jar"]
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
EXPOSE 8081
